﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashLight : MonoBehaviour
{
    void Update()
    {
        if (Input.GetKey(KeyCode.Q))
            this.GetComponent<Light>().enabled = true;
        if (Input.GetKey(KeyCode.E))
            this.GetComponent<Light>().enabled = false;
    }
}
